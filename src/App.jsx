import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Route, Routes, Navigate, useNavigate, useLocation } from 'react-router-dom';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CourseInfo from './components/CourseInfo/CourseInfo';
import CreateCourse from './components/CreateCourse/CreateCourse';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import { mockedCoursesList, mockedAuthorsList } from './data/mockData';

const App = () => {
  const [courses, setCourses] = useState(mockedCoursesList);
  const token = localStorage.getItem('token');

  const handleCreateCourse = (newCourse) => {
    setCourses([...courses, newCourse]);
  };

  return (
    <>
      <Header />
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
        <Route path="/courses" element={token ? <Courses courses={courses} authors={mockedAuthorsList} /> : <Navigate to="/login" />} />
        <Route path="/courses/add" element={token ? <CreateCourse onCreateCourse={handleCreateCourse} authors={mockedAuthorsList} /> : <Navigate to="/login" />} />
        <Route path="/courses/:courseId" element={token ? <CourseInfo courses={courses} authors={mockedAuthorsList} /> : <Navigate to="/login" />} />
        <Route path="/" element={token ? <Navigate to="/courses" /> : <Navigate to="/login" />} />
      </Routes>
    </>
  );
};

export default App;