import React from 'react';
import styled from 'styled-components';
import Button from '../Button/Button';

const AuthorItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
  margin-bottom: 10px;
`;

const AuthorName = styled.span`
  font-size: 16px;
`;

const AuthorItem = ({ author, onButtonClick, buttonLabel }) => {
  return (
    <AuthorItemContainer>
      <AuthorName>{author.name}</AuthorName>
      <Button buttonText={buttonLabel} onClick={() => onButtonClick(author.id)} />
    </AuthorItemContainer>
  );
};

export default AuthorItem;