import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import Input from '../input/input';
import Button from '../Button/Button';

const Form = styled.form`
  max-width: 600px;
  margin: 0 auto;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
  text-align: center;
`;

const ButtonContainer = styled.div`
  width: 50%;
  margin: 0 auto;
  margin-top: 20px;
`;

const InfoText = styled.p`
  margin-top: 20px;
`;

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const validate = () => {
    const newErrors = {};
    if (!email) newErrors.email = 'Email is required';
    if (!password) newErrors.password = 'Password is required';
    return newErrors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newErrors = validate();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    const user = { email, password };

    try {
      console.log('Sending login request with user:', user);
      const response = await fetch('http://localhost:4000/login', {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        console.error('Error response from server:', errorData);
        setErrors({ general: errorData.message });
        return;
      }

      const result = await response.json();
      console.log('Response from server:', result);
      if (result.successful) {
        const token = result.result; // Assuming 'result' contains the token
        const userName = result.user.name; // Assuming 'user' contains the user's name

        localStorage.setItem('token', token);
        localStorage.setItem('userName', userName);
        navigate('/courses');
      } else {
        setErrors({ general: 'Login failed' });
      }
    } catch (error) {
      console.error('Login failed with error:', error);
      setErrors({ general: 'Login failed' });
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <h2>Login</h2>
      {errors.general && <p style={{ color: 'red' }}>{errors.general}</p>}
      <Input
        labelText="Email"
        placeholderText="Enter your email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        hasError={!!errors.email}
        errorMessage={errors.email}
      />
      <Input
        labelText="Password"
        placeholderText="Enter your password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        hasError={!!errors.password}
        errorMessage={errors.password}
      />
      <ButtonContainer>
        <Button buttonText="Login" type="submit" />
      </ButtonContainer>
      <InfoText>If you don't have an account you may <a href="/registration">Register</a></InfoText>
    </Form>
  );
};

export default Login;
