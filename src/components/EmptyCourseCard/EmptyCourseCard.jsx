// src/components/EmptyCourseList.js
import React from 'react';
import styled from 'styled-components';
import Button from '../Button/Button';

const EmptyListContainer = styled.div`
  text-align: center;
  padding: 50px 20px;
`;

const Title = styled.h2`
  margin-bottom: 20px;
`;

const Subtitle = styled.p`
  margin-bottom: 20px;
  color: #6c757d;
`;

const EmptyCourseList = () => {
  return (
    <EmptyListContainer>
      <Title>Course List is Empty</Title>
      <Subtitle>Please use "Add New Course" button to add your first course.</Subtitle>
      <Button buttonText="Add New Course" onClick={() => {}} />
    </EmptyListContainer>
  );
};

export default EmptyCourseList;
