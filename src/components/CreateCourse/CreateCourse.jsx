// src/components/CreateCourse/CreateCourse.jsx

import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addCourse } from '../../store/courses/actions'; // Убедитесь, что путь к actions правильный
import { fetchAuthors } from '../../store/authors/actions'; // Убедитесь, что путь к actions правильный
import Input from '../input/input';
import Button from '../Button/Button';
import styled from 'styled-components';

const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 400px;
  margin: 0 auto;
`;

const CreateCourse = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [duration, setDuration] = useState(0);
  const [authors, setAuthors] = useState('');
  const [errors, setErrors] = useState({});
  const dispatch = useDispatch();
  const authorsList = useSelector((state) => state.authors.authorsList);

  useEffect(() => {
    dispatch(fetchAuthors());
  }, [dispatch]);

  const validate = () => {
    const newErrors = {};
    if (!title) newErrors.title = 'Title is required';
    if (!description) newErrors.description = 'Description is required';
    if (duration <= 0) newErrors.duration = 'Duration must be greater than 0';
    if (!authors) newErrors.authors = 'At least one author is required';
    return newErrors;
  };

  const handleAddCourse = () => {
    const newErrors = validate();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }
  
    const newCourse = {
      title,
      description,
      duration: parseInt(duration, 10),
      authors: authors.split(',').map(author => author.trim()),
    };
  
    console.log('Adding course:', newCourse);
  
    dispatch(addCourse(newCourse)).then((response) => {
      if (response.error) {
        console.error('Failed to add course:', response.error);
        setErrors({ general: response.error });
      } else {
        console.log('Course added successfully:', response.payload);
        setErrors({});
      }
    }).catch((error) => {
      console.error('Error occurred:', error);
      setErrors({ general: 'An unexpected error occurred' });
    });
  };
  

  return (
    <FormContainer>
      <h2>Create Course</h2>
      {errors.general && <p style={{ color: 'red' }}>{errors.general}</p>}
      <Input
        labelText="Title"
        placeholderText="Enter course title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        hasError={!!errors.title}
        errorMessage={errors.title}
      />
      <Input
        labelText="Description"
        placeholderText="Enter course description"
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        hasError={!!errors.description}
        errorMessage={errors.description}
      />
      <Input
        labelText="Duration"
        placeholderText="Enter course duration"
        type="number"
        value={duration}
        onChange={(e) => setDuration(e.target.value)}
        hasError={!!errors.duration}
        errorMessage={errors.duration}
      />
      <Input
        labelText="Authors (comma separated)"
        placeholderText="Enter course authors"
        value={authors}
        onChange={(e) => setAuthors(e.target.value)}
        hasError={!!errors.authors}
        errorMessage={errors.authors}
      />
      <Button buttonText="Add Course" onClick={handleAddCourse} />
    </FormContainer>
  );
};

export default CreateCourse;

