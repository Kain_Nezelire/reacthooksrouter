// src/components/Button.js
import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  padding: 10px 20px;
  background-color: #007298;
  color: white;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-size: 17px;
  &:hover {
    background-color: #007298;
  }
  &:focus {
    outline: none;
  }
`;

const Button = ({ buttonText, onClick, ...props }) => {
  return (
    <StyledButton onClick={onClick} {...props}>
      {buttonText}
    </StyledButton>
  );
};

export default Button;
