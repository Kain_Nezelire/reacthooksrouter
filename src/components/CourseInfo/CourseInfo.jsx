// src/components/CourseInfo.js
import React from 'react';
import styled from 'styled-components';
import { useParams, Link } from 'react-router-dom';
import { mockedCoursesList, mockedAuthorsList } from '../../data/mockData';

const CourseInfoContainer = styled.div`
  max-width: 800px;
  margin: 0 auto;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
`;

const Title = styled.h2`
  margin-top: 0;
`;

const Info = styled.p`
  margin-bottom: 10px;
`;

const BackButton = styled(Link)`
  display: inline-block;
  margin-top: 20px;
  padding: 10px 20px;
  background-color: #007bff;
  color: #fff;
  text-decoration: none;
  border-radius: 4px;

  &:hover {
    background-color: #0056b3;
  }
`;

const CourseInfo = () => {
  const { courseId } = useParams();
  const course = mockedCoursesList.find((c) => c.id === courseId);
  const authors = course.authors.map((authorId) => {
    const author = mockedAuthorsList.find((a) => a.id === authorId);
    return author ? author.name : '';
  }).join(', ');

  return (
    <CourseInfoContainer>
      <Title>{course.title}</Title>
      <Info><strong>Description:</strong> {course.description}</Info>
      <Info><strong>Duration:</strong> {course.duration} minutes</Info>
      <Info><strong>Authors:</strong> {authors}</Info>
      <Info><strong>Creation Date:</strong> {course.creationDate}</Info>
      <BackButton to="/courses">Back to Courses</BackButton>
    </CourseInfoContainer>
  );
};

export default CourseInfo;
