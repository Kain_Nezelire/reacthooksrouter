// src/components/Courses/Courses.jsx

import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import CourseCard from '../CourseCard/CourseCard';
import { fetchCourses } from '../../store/courses/actions'; // Обновите путь
import { fetchAuthors } from '../../store/authors/actions'; // Обновите путь
import Button from '../Button/Button';

const CoursesContainer = styled.div`
  padding: 20px;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 20px;
`;

const Courses = () => {
  const dispatch = useDispatch();
  const { coursesList, loading, error } = useSelector((state) => state.courses);
  const { authorsList } = useSelector((state) => state.authors);

  useEffect(() => {
    dispatch(fetchCourses());
    dispatch(fetchAuthors()); // Fetch authors along with courses
  }, [dispatch]);

  useEffect(() => {
    console.log('Fetched courses:', coursesList);
  }, [coursesList]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error: {error}</p>;

  return (
    <CoursesContainer>
      <ButtonContainer>
        <Link to="/courses/add">
          <Button buttonText="Add New Course" />
        </Link>
      </ButtonContainer>
      {Array.isArray(coursesList) && coursesList.length === 0 ? (
        <p>No courses available</p>
      ) : (
        Array.isArray(coursesList) && coursesList.map((course) => (
          <CourseCard key={course.id} course={course} authors={authorsList} />
        ))
      )}
    </CoursesContainer>
  );
};

export default Courses;
