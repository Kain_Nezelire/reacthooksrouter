import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import Input from '../input/input';
import Button from '../Button/Button';

const Form = styled.form`
  max-width: 600px;
  margin: 0 auto;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
  text-align: center;
`;

const ButtonContainer = styled.div`
  width: 50%;
  margin: 0 auto;
  margin-top: 20px;
`;

const InfoText = styled.p`
  margin-top: 20px;
`;

const Registration = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const validate = () => {
    const newErrors = {};
    if (!name) newErrors.name = 'Name is required';
    if (!email) newErrors.email = 'Email is required';
    if (!password) newErrors.password = 'Password is required';
    return newErrors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newErrors = validate();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    const newUser = { name, email, password };

    try {
      const response = await fetch('http://localhost:4000/register', {
        method: 'POST',
        body: JSON.stringify(newUser),
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        setErrors({ general: errorData.message });
        return;
      }

      const result = await response.json();
      if (result.success) {
        navigate('/login');
      } else {
        setErrors({ general: 'Registration failed' });
      }
    } catch (error) {
      setErrors({ general: 'Registration failed' });
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <h2>Registration</h2>
      {errors.general && <p style={{ color: 'red' }}>{errors.general}</p>}
      <Input
        labelText="Name"
        placeholderText="Enter your name"
        value={name}
        onChange={(e) => setName(e.target.value)}
        hasError={!!errors.name}
        errorMessage={errors.name}
      />
      <Input
        labelText="Email"
        placeholderText="Enter your email"
        type="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        hasError={!!errors.email}
        errorMessage={errors.email}
      />
      <Input
        labelText="Password"
        placeholderText="Enter your password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        hasError={!!errors.password}
        errorMessage={errors.password}
      />
      <ButtonContainer>
        <Button buttonText="Register" type="submit" />
      </ButtonContainer>
      <InfoText>If you have an account you may <a href="/login">Login</a></InfoText>
    </Form>
  );
};

export default Registration;
