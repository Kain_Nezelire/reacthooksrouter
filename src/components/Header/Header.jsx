import React from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import logo from '../../assets/Logo.png';
import Button from '../Button/Button';

const HeaderContainer = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
  background-color: #ffffff;
  border-bottom: 1px solid #e9ecef;
`;

const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  position: relative; /* Добавлено для позиционирования логотипа */
`;

const LogoImage = styled.img`
  position:absolute;
    left:50px;
    top:-25px;
`;

const LogoText = styled.span`
  margin-left: 80px; /* Увеличено для предотвращения перекрытия с логотипом */
  font-size: 20px;
  font-weight: bold;
  color: #333;
`;

const UserContainer = styled.div`
  display: flex;
  align-items: center;
`;

const UserName = styled.span`
  margin-right: 10px;
  font-size: 16px;
  color: #333;
`;

const Header = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const token = localStorage.getItem('token');
  const userName = localStorage.getItem('userName');

  const handleLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    navigate('/login');
  };

  const showHeader = !['/login', '/registration'].includes(location.pathname);

  return (
    <HeaderContainer>
      <LogoContainer>
        <LogoImage src={logo} alt="Logo" onClick={() => navigate('/')} />
      </LogoContainer>
      {showHeader && (
        <UserContainer>
          {token && <UserName>{userName}</UserName>}
          <Button buttonText="Logout" onClick={handleLogout} />
        </UserContainer>
      )}
    </HeaderContainer>
  );
};

export default Header;