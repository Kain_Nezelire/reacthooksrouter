import React from 'react';
import styled from 'styled-components';

const InputContainer = styled.div`
  margin-bottom: 15px;
  width: 100%;
`;

const Label = styled.label`
  display: block;
  margin-bottom: 5px;
  font-weight: bold;
`;

const InputField = styled.input`
  width: 80%;
  margin: 0 auto;
  padding: 10px;
  border: 1px solid ${({ $hasError }) => ($hasError ? 'red' : '#ccc')}; // Используем $ для фильтрации пропсов
  border-radius: 4px;
  font-size: 16px;
`;

const Error = styled.div`
  color: red;
  margin-top: 5px;
`;

const Input = ({ labelText, placeholderText, onChange, hasError, errorMessage, ...props }) => {
  return (
    <InputContainer>
      <Label>{labelText}</Label>
      <InputField placeholder={placeholderText} onChange={onChange} $hasError={hasError} {...props} /> 
      {hasError && <Error>{errorMessage}</Error>}
    </InputContainer>
  );
};

export default Input;
