// src/components/CourseCard/CourseCard.jsx

import React from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { deleteCourse } from '../../store/courses/actions'; // Обновите путь
import Button from '../Button/Button';

const Card = styled.div`
  border: 1px solid #ddd;
  padding: 20px;
  margin-bottom: 20px;
  border-radius: 8px;
`;

const Title = styled.h3`
  margin: 0;
`;

const Description = styled.p`
  margin: 10px 0;
`;

const Authors = styled.p`
  margin: 10px 0;
`;

const CourseCard = ({ course, authors }) => {
  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(deleteCourse(course.id));
  };

  const getCourseAuthors = (authorIds) => {
    if (!Array.isArray(authors)) {
      return 'Unknown authors';
    }
    return authorIds
      .map((authorId) => {
        const author = authors.find((author) => author.id === authorId);
        return author ? author.name : 'Unknown';
      })
      .join(', ');
  };

  return (
    <Card>
      <Title>{course.title}</Title>
      <Description>{course.description}</Description>
      <p>Duration: {course.duration} hours</p>
      <Authors>Authors: {getCourseAuthors(course.authors)}</Authors>
      <Button buttonText="Delete" onClick={handleDelete} />
      <Button buttonText="Update" onClick={() => {/* Add update logic here */}} />
    </Card>
  );
};

export default CourseCard;
