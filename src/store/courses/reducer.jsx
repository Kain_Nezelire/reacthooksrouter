// src/store/courses/reducer.js

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchCourses = createAsyncThunk('courses/fetchCourses', async () => {
  const response = await fetch('http://localhost:4000/courses/all');
  const data = await response.json();
  return data;
});

export const addCourse = createAsyncThunk('courses/addCourse', async (newCourse) => {
  const token = localStorage.getItem('token');
  const response = await fetch('http://localhost:4000/courses/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    },
    body: JSON.stringify(newCourse),
  });
  const data = await response.json();
  return data;
});

export const deleteCourse = createAsyncThunk('courses/deleteCourse', async (courseId) => {
  const token = localStorage.getItem('token');
  const response = await fetch(`http://localhost:4000/courses/${courseId}`, {
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  });
  const data = await response.json();
  return data;
});

const coursesSlice = createSlice({
  name: 'courses',
  initialState: {
    coursesList: [], // начальное состояние - массив
    loading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCourses.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchCourses.fulfilled, (state, action) => {
        state.loading = false;
        state.coursesList = Array.isArray(action.payload) ? action.payload : []; // проверка, что payload - массив
      })
      .addCase(fetchCourses.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(addCourse.fulfilled, (state, action) => {
        state.coursesList.push(action.payload);
      })
      .addCase(deleteCourse.fulfilled, (state, action) => {
        state.coursesList = state.coursesList.filter(course => course.id !== action.payload.id);
      });
  },
});

export default coursesSlice.reducer;
