// src/store/courses/actions.js

export const setCourses = (courses) => ({
    type: 'SET_COURSES',
    payload: courses,
  });
  
  export const fetchCourses = () => async (dispatch) => {
    const response = await fetch('http://localhost:4000/courses/all');
    const data = await response.json();
    dispatch(setCourses(data));
  };
  
  export const addCourse = (newCourse) => async (dispatch) => {
    const token = localStorage.getItem('token');
    console.log('Adding course with token:', token);
    console.log('Course data:', newCourse);
    try {
      const response = await fetch('http://localhost:4000/courses/add', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify(newCourse),
      });
  
      const result = await response.json();
      console.log('Server response:', result);
  
      if (!response.ok || !result.successful) {
        console.error('Error response from server:', result);
        throw new Error(result.message || 'Failed to add course');
      }
  
      dispatch(fetchCourses()); // Reload courses after successful addition
      return result;
    } catch (error) {
      console.error('Failed to add course:', error);
      return { error: error.message };
    }
  };
  
  
  export const deleteCourse = (courseId) => async (dispatch) => {
    const token = localStorage.getItem('token');
    console.log('Deleting course with token:', token);
    try {
      const response = await fetch(`http://localhost:4000/courses/${courseId}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
  
      const result = await response.json();
      console.log('Server response:', result);
  
      if (!response.ok) {
        console.error('Error response from server:', result);
        throw new Error(result.message || 'Failed to delete course');
      }
  
      dispatch(fetchCourses()); // Reload courses after successful deletion
      return result;
    } catch (error) {
      console.error('Failed to delete course:', error);
      return { error: error.message };
    }
  };
  