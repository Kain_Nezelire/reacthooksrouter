// src/store/authors/actions.js

export const setAuthors = (authors) => ({
    type: 'SET_AUTHORS',
    payload: authors,
  });
  
  export const fetchAuthors = () => async (dispatch) => {
    const response = await fetch('http://localhost:4000/authors/all');
    const data = await response.json();
    dispatch(setAuthors(data));
  };
  