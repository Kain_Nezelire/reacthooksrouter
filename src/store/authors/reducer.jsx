import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// Fetch all authors
export const fetchAuthors = createAsyncThunk('authors/fetchAuthors', async () => {
  const response = await fetch('http://localhost:4000/authors/all');
  const data = await response.json();
  return data;
});

const authorsSlice = createSlice({
  name: 'authors',
  initialState: {
    authorsList: [],
    loading: false,
    error: null,
  },
  reducers: {
    setAuthors: (state, action) => {
      state.authorsList = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAuthors.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchAuthors.fulfilled, (state, action) => {
        state.loading = false;
        console.log('Fetched authors:', action.payload); // Log the fetched authors
        state.authorsList = action.payload; // Assuming payload is an array of authors
      })
      .addCase(fetchAuthors.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const { setAuthors } = authorsSlice.actions;

export default authorsSlice.reducer;
