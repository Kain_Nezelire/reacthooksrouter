import { configureStore } from '@reduxjs/toolkit';
import coursesReducer from './courses/reducer';
import authorsReducer from './authors/reducer';

const store = configureStore({
  reducer: {
    courses: coursesReducer,
    authors: authorsReducer,
  },
});

export default store;
