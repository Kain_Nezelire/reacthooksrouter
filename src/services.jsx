// src/services.js
import axios from 'axios';

// Создаем инстанс axios с базовым URL
const api = axios.create({
  baseURL: 'http://localhost:4000', // базовый URL API
  headers: {
    'Content-Type': 'application/json'
  }
});

// Устанавливаем токен авторизации (при необходимости)
export const setAuthToken = (token) => {
  api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

// Получение всех курсов
export const fetchCourses = async () => {
  const response = await api.get('/courses/all');
  return response.data;
};

// Получение всех авторов
export const fetchAuthors = async () => {
  const response = await api.get('/authors/all');
  return response.data;
};

// Создание нового курса
export const createCourse = async (course) => {
  const response = await api.post('/courses', course);
  return response.data;
};

// Создание нового автора
export const createAuthor = async (author) => {
  const response = await api.post('/authors', author);
  return response.data;
};
